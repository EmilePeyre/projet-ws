package fr.ib.emile.mediatheque3ejb;

import java.time.LocalTime;

import javax.ejb.Remote;

@Remote
public interface IDvdvtheque {
	
	public String getInfos();
	public boolean ouvertA(LocalTime time);
	public LocalTime getDerniereInterrogation();
}
