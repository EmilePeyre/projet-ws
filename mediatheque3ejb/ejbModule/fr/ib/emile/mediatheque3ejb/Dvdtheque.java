package fr.ib.emile.mediatheque3ejb;

import java.time.LocalTime;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.Stateless;

@Stateful(name = "Dvds", description = "Opération pour la Dvdthèque")
public class Dvdtheque implements IDvdvtheque {
	
	private IDvdDAO dvdDao;
	
	private LocalTime derniereInterrogation;
	

	public String getInfos() {
		return "Nouvelle DVDthèque ouverte de 10h à 18h";
	}
	
	public boolean ouvertA(LocalTime time) {
		derniereInterrogation = time;
//		int h = time.getHour();
//		return (h>10 && h <18); 
		return (time.isAfter(LocalTime.of(10, 0)) && time.isBefore(LocalTime.of(18, 0)));
	}

	public LocalTime getDerniereInterrogation() {
		return derniereInterrogation;
	}

	//@EJB(lookup = "ejb:/Mediatheque3Ejb/Dvds!fr.ib.emile.mediatheque3ejb.IDvdvDAO")
	@EJB(name="DvdDAO")
	public void setDvdDao(IDvdDAO dvdDao) {
		this.dvdDao = dvdDao;
	}
}
