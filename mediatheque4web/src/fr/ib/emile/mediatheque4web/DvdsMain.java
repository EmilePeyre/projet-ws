package fr.ib.emile.mediatheque4web;

import java.time.LocalTime;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import fr.ib.emile.mediatheque3ejb.Dvd;
import fr.ib.emile.mediatheque3ejb.IDvdDAO;
import fr.ib.emile.mediatheque3ejb.IDvdvtheque;

public class DvdsMain {

	public static void main(String[] args) {
		System.out.println("Dvd : client lourd");
		try {
			Context context = new InitialContext();
			IDvdvtheque dvdvtheque = (IDvdvtheque) context.lookup("ejb:/Mediatheque3Ejb/Dvds!fr.ib.emile.mediatheque3ejb.IDvdvtheque");
			System.out.println("Informations : "+dvdvtheque.getInfos());
			System.out.println("Ouvert maintenant : "+dvdvtheque.ouvertA(LocalTime.now()));
			
			// ajout de DVD avec le DAO
			IDvdDAO dvdDAO = (IDvdDAO) context.lookup("ejb:/Mediatheque3Ejb/DvdDAO!fr.ib.emile.mediatheque3ejb.IDvdDAO");
			dvdDAO.ajouter(new Dvd("Monty Python : Sacré Graal !", 1975));
			dvdDAO.ajouter(new Dvd("La soupe aux choux", 1981));
			dvdDAO.ajouter(new Dvd("OSS 117 : Le Caire, nid d'espions", 2006));
			System.out.println("Il y a actuellement "+dvdDAO.getNombre()+" DVD");
			
			context.close();
		} catch (NamingException e) {
			System.err.println("Erreur : "+e);
		}
	}

}
