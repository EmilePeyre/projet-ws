package fr.ib.emile.mediatheque4web;

import java.io.IOException;
import java.io.Writer;
import java.time.LocalTime;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ib.emile.mediatheque3ejb.Dvd;
import fr.ib.emile.mediatheque3ejb.IDvdDAO;
import fr.ib.emile.mediatheque3ejb.IDvdvtheque;

@WebServlet("/dvds")
public class DvdsServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private IDvdvtheque dvdtheque;
	private IDvdDAO dvdDAO;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		Writer out = resp.getWriter();

		out.write("<!DOCTYPE html><html><body>");
		out.write("<h1>DVDthèque</h1>");
		out.write("</body></html>");
		out.write("<p>Ouvert à 9h : "+dvdtheque.ouvertA(LocalTime.of(9, 0))+"</p>");
		out.write("<p>Dernière interrogation : "+dvdtheque.getDerniereInterrogation()+"</p>");
		
		List<Dvd> dvdList = dvdDAO.lireTous();
		out.write("<h2>Liste des DVD</h2>");
		out.write("<table><tr><th>Titre</th><th>Année</th></tr>");
		for (Dvd dvd: dvdList) out.write("<tr><td>"+dvd.getTitre()+"</td><td>"+dvd.getAnnee()+"</td></tr>");
		out.write("</table>");
		
		out.close();

	}

	@EJB(lookup = "ejb:/Mediatheque3Ejb/Dvds!fr.ib.emile.mediatheque3ejb.IDvdvtheque")
	public void setDvdtheque(IDvdvtheque dvdtheque) {
		this.dvdtheque = dvdtheque;
	}
	@EJB(lookup = "ejb:/Mediatheque3Ejb/DvdDAO!fr.ib.emile.mediatheque3ejb.IDvdDAO")
	public void setDvdDAO(IDvdDAO dvdDAO) {
		this.dvdDAO = dvdDAO;
	}
}
