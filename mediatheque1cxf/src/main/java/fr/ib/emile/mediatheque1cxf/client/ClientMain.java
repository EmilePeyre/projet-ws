package fr.ib.emile.mediatheque1cxf.client;

import java.time.LocalDate;

import javax.xml.ws.soap.SOAPFaultException;

import org.apache.cxf.binding.soap.SoapFault;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import fr.ib.emile.mediatheque1cxf.serveur.ILivresService;

public class ClientMain {

	public static void main(String[] args) {
		System.out.println("Client livres");
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setAddress("http://localhost:9001/livres");
		factory.setServiceClass(ILivresService.class);
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		factory.getInInterceptors().add(new LoggingInInterceptor());
		ILivresService livresService = factory.create(ILivresService.class);
		System.out.println(livresService.getInfos());
		System.out.println("Livre 4 empruntable : " + livresService.estEmpruntable(4));
		try {
			System.out.println("Livre -3 empruntable : " + livresService.estEmpruntable(-3));
		} catch (Exception e) {
			System.out.println("Erreur : " + e);
		}
		System.out.println("Retour livre 4 : "+livresService.getRetour(4));
		System.out.println("Livre du mois : "+livresService.getLivreDuMois());
		System.out.println("Livre du mois de mars : "+livresService.getLivresDeLannee()[2]);
		
	}

}
