package fr.ib.emile.mediatheque1cxf.serveur;


import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.jws.WebService;

@WebService
public class LivresService implements ILivresService {
	
	public String getInfos() {
		return "L'entrée de la bibliothèque se trouve dans votre coeur";
	}
	
	public boolean estEmpruntable(int id) {
		if (id<1) {
			throw new IllegalArgumentException("id doit être supérieur à 1");
		}
		return false;
	}
	
	public Date getRetour(int id) {
		LocalDate retour = LocalDate.now().plusDays(10);
		return java.util.Date.from(retour.atStartOfDay()
			      .atZone(ZoneId.systemDefault())
			      .toInstant());
	}
	
	public Livre getLivreDuMois() {
		Livre livre = new Livre("T'choupi fait la sieste", "T. Courtin", 2017);
//		DataSource dataSource = new FileDataSource("tchoupi.jpg");
//		DataHandler dataHandler = new DataHandler(dataSource);
//		livre.setImage(dataHandler);
		return livre;
	}
	
	public Livre[] getLivresDeLannee() {
		Livre[] livres = new Livre[12];
		for (int i=0; i<12; i++) {
			livres[i]= getLivreDuMois();
		}
		return livres;
	}

}
