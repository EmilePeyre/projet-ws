package fr.ib.emile.mediatheque2spring.serveur;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class CdService {
	
	private List<Cd> cds;
	
	public CdService() {
		cds = new ArrayList<Cd>();
	}
	
	public int getNombreDeCd() {
		return cds.size();
	}
	
	public void ajouteCd(Cd cd) {
		cds.add(cd);
	}
	
	public Cd getCd(int id) {
		return cds.get(id);
	}

	public List<Cd> getCds() {
		return cds;
	}

	public void setCds(List<Cd> cds) {
		this.cds = cds;
	}

}
