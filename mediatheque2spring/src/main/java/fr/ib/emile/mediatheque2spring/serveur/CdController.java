package fr.ib.emile.mediatheque2spring.serveur;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CdController {
	
	private CdService cdService;
	
	@RequestMapping(path="/cd/nombre", produces="text/xml")
	public int getNombre() {
		return cdService.getNombreDeCd();
	}

	@RequestMapping(path="/cd", method=RequestMethod.POST)
	public void ajoute(@RequestBody Cd cd) {
		cdService.ajouteCd(cd);
	}
	
	@RequestMapping(path="/cd", method=RequestMethod.GET)
	public List<Cd> getTous(){
		return cdService.getCds();
	}
	
	@RequestMapping(path="/json/cd", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Cd> getTousJson(){
		return cdService.getCds();
	}
	
	// num est composé d'un ou plusieurs (+) chiffres (\\d)
	@RequestMapping(path="/cd/{num:\\d+}", method=RequestMethod.GET)
	public Cd getUn(@PathVariable("num") int id) {
		return cdService.getCd(id);
	}
	
	@RequestMapping(path="/cd/{num:\\d}/titre", method=RequestMethod.PUT)
	public void changeTitre(@PathVariable("num") int id, @RequestBody String titre) {
		cdService.getCd(id).setTitre(titre);
	}
	
	@RequestMapping(path="/cd/reset", method = RequestMethod.POST)
	public void resetCds() {
		cdService.getCds().clear();
	}

	@Autowired
	public void setCdService(CdService cdService) {
		this.cdService = cdService;
	}

}
