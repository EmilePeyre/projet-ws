package fr.ib.emile.mediatheque2spring.serveur;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

	@RequestMapping("/health")
	public String getHealth() {
		return "ok";
	}
	
}
