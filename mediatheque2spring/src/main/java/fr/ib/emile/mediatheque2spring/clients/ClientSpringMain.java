package fr.ib.emile.mediatheque2spring.clients;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.ib.emile.mediatheque2spring.serveur.Cd;

public class ClientSpringMain {

	public static void main(String[] args) {
		try {
			System.out.println("Client Java Spring");
			RestTemplate rt = new RestTemplate();
			String health = rt.getForObject("http://localhost:9002/health", String.class);
			System.out.println("Health : " + health);
			
			// Reset de la liste des CD
			rt.execute("http://localhost:9002/cd/reset", HttpMethod.POST, null, null);
			System.out.println("Médiathèque vidée");
			
			// Ajout d'un CD
			Cd cdAjout = new Cd("Simmulation Theory", "Muse", 2018);
			rt.postForObject("http://localhost:9002/cd", cdAjout, Void.class);
			System.out.println("Ajout de "+cdAjout);
			
			cdAjout = new Cd("Les failles cachées", "Pomme", 2020);
			rt.postForObject("http://localhost:9002/cd", cdAjout, Void.class);
			System.out.println("Ajout de "+cdAjout);
			
			cdAjout = new Cd("Robots après tout", "Philippe Katerine", 2005);
			rt.postForObject("http://localhost:9002/cd", cdAjout, Void.class);
			System.out.println("Ajout de "+cdAjout);

			// Nombre de CD enregistrés
			int nbCd = rt.getForObject("http://localhost:9002/cd/nombre", Integer.class);
			System.out.println("Il y a "+nbCd+" CD dans la médiathèque.");
			
			// Récupération de la liste des CD
//			// solution 1 : on fait des trucs chelous pour récupérer une List<Cd>
//			
//			ParameterizedTypeReference<List<Cd>> ref = new ParameterizedTypeReference<List<Cd>>() {};
//			ResponseEntity<List<Cd>> cdsEntity = rt.exchange("http://localhost:9002/cd", HttpMethod.GET, null, ref);
//			List<Cd> cdList = cdsEntity.getBody();
//			System.out.println("Liste des CD dans la médiathèque :");
//			for (Cd cd : cdList) {
//				System.out.println(cd);
//			}
			
			// solution 2 : on récupère un array à la place
			Cd[] cds = rt.getForObject("http://localhost:9002/cd/", Cd[].class);
			System.out.println("Liste des CD dans la médiathèque :");
			for (Cd cd : cds) System.out.println(cd);
			
			// Changement de titre d'un CD
			System.out.println("Changement de titre");
			rt.put("http://localhost:9002/cd/0/titre", "Simulation Theory", String.class);
			
			// Récupération d'un CD
			Cd cdGet = rt.getForObject("http://localhost:9002/cd/0", Cd.class);
			System.out.println("Premier CD : "+cdGet);
		} catch (Exception e) {
			System.err.println("Erreur : " + e);
		}
	}
}
